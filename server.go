package main

import (
	"errors"
	"github.com/gorilla/websocket"
	"golang.org/x/image/bmp"
	"image"
	"image/color"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var (
	img *image.RGBA
)

// use map to get O(1)
var apiKeys = make(map[string]int)
var currSec = time.Now().Second()

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}
var rateLimitPX = 1036800

func main() {
	width := 1900
	height := 1080

	upLeft := image.Point{0, 0}
	lowRight := image.Point{width, height}

	img = image.NewRGBA(image.Rectangle{upLeft, lowRight})

	go render()
	go resetIntervall()
	loadKeys()
	webSocketWorker()
}

func resetIntervall() {
	currSec := time.Now().Second()
	for {
		for currSec == time.Now().Second() {
		}
		currSec = time.Now().Second()
		resetTime()
	}
}

func resetTime() {
	for key := range apiKeys {
		apiKeys[key] = 0
	}
}

func loadKeys() {
	dat, err := ioutil.ReadFile("keys.txt")
	if err != nil {
		panic(err)
	}
	for _, apiKey := range strings.Split(string(dat), "\n") {
		if len(apiKey) > 35 {
			apiKeys[apiKey[0:36]] = 0
		}
	}
}

func processPixel(args []string) error {
	x, errX := strconv.Atoi(args[0])
	y, errY := strconv.Atoi(args[1])
	color, errColor := hexToColor(args[2])
	if errX != nil {
		return errors.New("Bad X")
	} else if errY != nil {
		return errors.New("Bad Y")
	} else if errColor != nil {
		return errColor
	} else {
		img.Set(x, y, color)
		return nil
	}
}

func hexToColor(hex string) (color.RGBA, error) {
	red, err0 := strconv.ParseInt(hex[1:3], 16, 16)
	green, err1 := strconv.ParseInt(hex[3:5], 16, 16)
	blue, err2 := strconv.ParseInt(hex[5:7], 16, 16)
	if err0 != nil || err1 != nil || err2 != nil {
		return color.RGBA{0x00, 0x00, 0x00, 0x00}, errors.New("Cannot color")
	} else {
		return color.RGBA{byte(red), byte(green), byte(blue), 0xff}, nil
	}
}

func getCmdAndArgs(line string) (cmd string, args []string) {
	lineSplitted := strings.Split(line, " ")
	args = make([]string, 0)
	if len(lineSplitted) > 1 {
		args = lineSplitted[1:]
	}
	return lineSplitted[0], args
}

func getApiKey(line string) (apiKey string, err error) {
	cmd, args := getCmdAndArgs(line)
	if cmd == "API" && len(args) > 0 {
		if _, exists := apiKeys[args[0]]; exists {
			return args[0], nil
		}
	}
	return "", errors.New("No API Key")
}

func closeConnection(conn *websocket.Conn, errorText string) {
	conn.WriteMessage(1, []byte(errorText))
	conn.Close()
}

func webSocketWorker() {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	http.HandleFunc("/px", func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		apiKey := ""
		if err == nil {
			for {
				_, msg, err := conn.ReadMessage()
				lines := strings.Split(string(msg), "\n")
				if err != nil {
					return
				}
				if apiKey == "" {
					var err error
					apiKey, err = getApiKey(lines[0])
					if err != nil {
						conn.Close()
					}
				}

				if apiKey != "" {
					if apiKeys[apiKey]+len(lines) < rateLimitPX {
						apiKeys[apiKey] += len(lines)
						for _, line := range lines {
							cmd, args := getCmdAndArgs(line)
							if cmd == "PX" && len(args) >= 3 {
								err := processPixel(args)
								if err != nil {
									conn.WriteMessage(1, []byte(err.Error()))
								}
							}
						}
					} else {
						conn.WriteMessage(1, []byte("rateLimit"))
					}
				} else {
					closeConnection(conn, "Missing API Key")
				}
			}
		}
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "websockets.html")
	})

	http.ListenAndServe("0.0.0.0:8080", nil)
}

func render() {
	curr := 0
	currSec := time.Now().Second()
	for {
		if currSec != time.Now().Second() {
			currSec = time.Now().Second()
			curr = 0
		}
		if curr < 25 {
			curr++
			bmp.Encode(os.Stdout, img)
		}
	}
}
